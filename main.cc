#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include "legion.h"

#include "params.hpp"
#include "legionvector.hpp"
#include "ell_sparsematrix.hpp"
#include "cgoperators.hpp"
#include "cgsolver.hpp"

using namespace LegionRuntime::HighLevel;

enum TaskIDs {
   TOP_LEVEL_TASK_ID = 0,
};

void top_level_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime){

	int64_t nx = 16; 
	int64_t nparts = 1;
    	const InputArgs &command_args = HighLevelRuntime::get_input_args();
    	if (command_args.argc > 1) {
    
         	nx = atoi(command_args.argv[1]);
         
         	if(command_args.argc > 2)
         		nparts = atoi(command_args.argv[2]);
   	}
   
	std::cout<<"Poisson eqn. will be solved over a square domain with "<<nx<<"x"<<nx<<
	" internal grid points."<<std::endl;
	std::cout<<std::endl;

   	int64_t size = nx * nx;
   	
   	Params<double> params(nx);
   	params.GenerateVals();

	std::cout<<"building unknown vector x..."<<std::endl;
	// build unknown vector   	
	Array<double> x(size, nparts, ctx, runtime);
	x.Initialize(ctx, runtime);

	std::cout<<"building rhs vector..."<<std::endl;
	// build rhs vector
	Array<double> b(size, nparts, ctx, runtime);
	b.Initialize(params.rhs, ctx, runtime);	
	
	std::cout<<"building sparse matrix..."<<std::endl;
	// build sparse matrix
	SpMatrix A(size, nparts, params.nonzeros, params.max_nzeros, ctx, runtime);
	A.BuildMatrix(params.vals, params.col_ind, params.nzeros_per_row, ctx, runtime);

	std::cout<<"and now solve the system using CG solver..."<<std::endl;	
	std::cout<<std::endl;

	// run CG solver
	CGSolver<double> cgsolver;
	bool result = cgsolver.Solve(A, b, x, -1, 1e-10, ctx, runtime);

	if(result) {
		std::cout<<"Converged! Number of Itr="<<cgsolver.GetNumberIterations()<<
		" ,  L2norm="<<std::setprecision(16)<<cgsolver.GetL2Norm()<<std::endl;
	}
	else {
		std::cout<<"NO CONVERGENCE!"<<std::endl;
	}

	// destroy the objects
	x.DestroyArray(ctx, runtime);
	b.DestroyArray(ctx, runtime);
	A.DestroySpMatrix(ctx, runtime);
	
	return;
}

int main(int argc, char **argv){

	HighLevelRuntime::set_top_level_task_id(TOP_LEVEL_TASK_ID);
   	HighLevelRuntime::register_legion_task<top_level_task>(TOP_LEVEL_TASK_ID,
       	Processor::LOC_PROC, true/*single*/, false/*index*/);
	
	RegisterVectorTask<double>();

	RegisterOperatorTasks<double>();	
 
  return HighLevelRuntime::start(argc, argv);
}
