#ifndef cgsolver_hpp
#define cgsolver_hpp

#include <iostream>
#include <cstdio>
#include <cassert>
#include <cstdlib>

#include "ell_sparsematrix.hpp"
#include "legionvector.hpp"

template<typename T>
class CGSolver{

	private:
	int niter;
	T L2normr;
	
	public:
	bool Solve(const SpMatrix &A,
		   const Array<T> &b,
		   Array<T> &x,
		   int nitermax,
		   T threshold,
		   Context ctx,
		   HighLevelRuntime *runtime);

	int GetNumberIterations(void) { return niter;}
	T GetL2Norm(void) { return L2normr;}
};

template<typename T>
bool CGSolver<T>::Solve(const SpMatrix &A,
                   const Array<T> &b,
                   Array<T> &x,
                   int nitermax,
                   T threshold,
		   Context ctx, 
		   HighLevelRuntime *runtime)
{

	assert(A.nrows == b.size);
	assert(b.size == x.size);
		   
	if(nitermax == -1) nitermax = A.nrows;

	Array<T> r_old(x.size, x.nparts, ctx, runtime);
	Array<T> r_new(x.size, x.nparts, ctx, runtime);
	Array<T> p(x.size, x.nparts, ctx, runtime);
	Array<T> A_p(x.size, x.nparts, ctx, runtime);

	// Ap = A * x	
	spmv(A, x, A_p, ctx, runtime);


	// r_old = b - Ap
	subtract(b, A_p, r_old, T(1.0), ctx, runtime);

	// Initial norm
	const T L2normr0 = L2norm(r_old, ctx, runtime);

	// p = r_old
	copy(r_old, p, ctx, runtime);

	niter = 0;
	std::cout<<"start iterating..."<<std::endl;
	while(niter < nitermax){
		
		niter++;
		std::cout<<niter<<std::endl;

		// Ap = A * p
		spmv(A, p, A_p, ctx, runtime);

		// r2 = r' * r
		T r2 = dot(r_old, r_old, ctx, runtime);

		T alpha = r2 / dot(p, A_p, ctx, runtime);		
	
		// x = x + alpha * p
		add(x, p, x, alpha , ctx, runtime);
	
		// r_new = r_old - alpha * A_p
		subtract(r_old , A_p, r_new, alpha, ctx, runtime);

		L2normr = L2norm(r_new, ctx, runtime);

		if(L2normr/ L2normr0 < threshold) return(true);

		T beta = dot(r_new, r_new, ctx, runtime) / r2;
		
		// r_new + beta*p
		add(r_new, p, p, beta, ctx, runtime);

		copy(r_old, r_new, ctx, runtime);
	}

	return(false);
} 	

#endif
