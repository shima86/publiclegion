#ifndef cgoperators_hpp
#define cgoperators_hpp

#include <iostream>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include "legion.h"

#include "legionvector.hpp"
#include "ell_sparsematrix.hpp"

using namespace LegionRuntime::HighLevel;
using namespace LegionRuntime::Accessor;

enum TaksIDs{
	SPMV_TASK_ID = 3,
	SUBTRACT_TASK_ID = 4,
	COPY_TASK_ID = 5,
	DOT_TASK_ID = 6,
	ADD_TASK_ID = 7,
	L2NORM_TASK_ID = 8,
	
};

enum OpIDs{
	REDUCE_ID = 1,
};

template<typename T>
class TaskArgs1{

	public:
	int scalar;
	FieldID A_row_fid;
	FieldID A_col_fid;
	FieldID A_val_fid;
	FieldID x_fid;
	FieldID Ax_fid;

	TaskArgs1(void){};
	TaskArgs1(const SpMatrix &A, const Array<T> &x, Array<T> &Ax, int64_t scalar){
		
		this-> scalar = scalar;
		this-> A_row_fid = A.row_fid;
		this-> A_col_fid = A.col_fid;
		this-> A_val_fid = A.val_fid;
		this-> x_fid = x.fid;
		this-> Ax_fid = Ax.fid;
	}

};

template<typename T>
class TaskArgs2{
	
	public:
	T scalar;
	FieldID x_fid;
	FieldID y_fid;
	FieldID z_fid;

	TaskArgs2(void);
	TaskArgs2(const Array<T> &x, const Array<T> &y, Array<T> &z, T scalar){

		this-> scalar = scalar;
		this-> x_fid = x.fid;
		this-> y_fid = y.fid;
		this-> z_fid = z.fid;
	}

	TaskArgs2(const Array<T> &x, Array<T> &y){
		
		this-> x_fid = x.fid;
                this-> y_fid = y.fid;
		this-> scalar = 0.0;
	}

};

// Reduction Op
class FutureSum {
	
  public:

  typedef double LHS;
  typedef double RHS;
  static const double identity;

  template <bool EXCLUSIVE> static void apply(LHS &lhs, RHS rhs);

  template <bool EXCLUSIVE> static void fold(RHS &rhs1, RHS rhs2);
};

const double FutureSum::identity = 0.0;

template<>
void FutureSum::apply<true>(LHS &lhs, RHS rhs)
{
  lhs += rhs;
}

template<>
void FutureSum::apply<false>(LHS &lhs, RHS rhs)
{
  int *target = (int *)&lhs;
  union { int as_int; double as_T; } oldval, newval;
  do {
    oldval.as_int = *target;
    newval.as_T = oldval.as_T + rhs;
  } while (!__sync_bool_compare_and_swap(target, oldval.as_int, newval.as_int));
}

template<>
void FutureSum::fold<true>(RHS &rhs1, RHS rhs2)
{
  rhs1 += rhs2;
}

template<>
void FutureSum::fold<false>(RHS &rhs1, RHS rhs2)
{
  int *target = (int *)&rhs1;
  union { int as_int; double as_T; } oldval, newval;
  do {
    oldval.as_int = *target;
    newval.as_T = oldval.as_T + rhs2;
  } while (!__sync_bool_compare_and_swap(target, oldval.as_int, newval.as_int));
}


// A_x = A * x
template<typename T>
void spmv(const SpMatrix &A, const Array<T> &x, Array<T> &A_x, Context ctx,  HighLevelRuntime *runtime){
	
	
	ArgumentMap arg_map;

	TaskArgs1<T> spmv_args(A, x, A_x, A.max_nzeros);

	IndexLauncher spmv_launcher(SPMV_TASK_ID, x.color_domain,
				    TaskArgument(&spmv_args, sizeof(spmv_args)), arg_map);

	spmv_launcher.add_region_requirement(
			RegionRequirement(A.row_lp, 0, READ_ONLY, EXCLUSIVE, A.row_lr));
	spmv_launcher.region_requirements[0].add_field(A.row_fid);

	spmv_launcher.add_region_requirement(
                        RegionRequirement(A.elem_lp, 0, READ_ONLY, EXCLUSIVE, A.elem_lr));
	spmv_launcher.region_requirements[1].add_field(A.val_fid);
	spmv_launcher.region_requirements[1].add_field(A.col_fid); 

	// Note: all elements of vector x is given to each process
	spmv_launcher.add_region_requirement(
                        RegionRequirement(x.lr, 0, READ_ONLY, EXCLUSIVE, x.lr));
        spmv_launcher.region_requirements[2].add_field(x.fid);

	spmv_launcher.add_region_requirement(
                        RegionRequirement(A_x.lp, 0, WRITE_DISCARD, EXCLUSIVE, A_x.lr));
        spmv_launcher.region_requirements[3].add_field(A_x.fid);

	runtime->execute_index_space(ctx, spmv_launcher);
	
	return;
}

template<typename T>
void spmv_task(const Task *task,
	       const std::vector<PhysicalRegion> &regions,
	       Context ctx, HighLevelRuntime *runtime){

	assert(regions.size() == 4);
        assert(task->regions.size() == 4);

        const  TaskArgs1<T> task_args = *((const TaskArgs1<T>*)task->args);

	int max_nzeros = task_args.scalar;

        RegionAccessor<AccessorType::Generic, int64_t> acc_num_nzeros =
        regions[0].get_field_accessor(task_args.A_row_fid).template typeify<int64_t>();

        RegionAccessor<AccessorType::Generic, int64_t> acc_col =
        regions[1].get_field_accessor(task_args.A_col_fid).template typeify<int64_t>();

        RegionAccessor<AccessorType::Generic, T> acc_vals =
        regions[1].get_field_accessor(task_args.A_val_fid).template typeify<T>();

	RegionAccessor<AccessorType::Generic, T> acc_x =
        regions[2].get_field_accessor(task_args.x_fid).template typeify<T>();

	RegionAccessor<AccessorType::Generic, T> acc_Ax =
        regions[3].get_field_accessor(task_args.Ax_fid).template typeify<T>();

        Domain row_dom = runtime->get_index_space_domain(ctx,
                         task->regions[0].region.get_index_space());
        Rect<1> row_rect = row_dom.get_rect<1>();

        Domain elem_dom = runtime->get_index_space_domain(ctx,
                          task->regions[1].region.get_index_space());
        Rect<1> elem_rect = elem_dom.get_rect<1>();

	// apply matrix vector multiplication
	{
		T sum;	
		DomainPoint pir;
		pir.dim = 1;

		GenericPointInRectIterator<1> itr1(row_rect);
		GenericPointInRectIterator<1> itr2(elem_rect);

		for(int i=0; i < row_rect.volume(); i++){

			sum = 0.0;
			int limit = acc_num_nzeros.read(DomainPoint::from_point<1>(itr1.p));

			for(int counter=0; counter < limit; counter++){

				int ind = acc_col.read(DomainPoint::from_point<1>(itr2.p));
				pir.point_data[0] = ind;
			
				sum += acc_vals.read(DomainPoint::from_point<1>(itr2.p)) * acc_x.read(pir);
				itr2++;
			}
			
			acc_Ax.write(DomainPoint::from_point<1>(itr1.p), sum);
			itr1++;
			itr2.p.x[0] = itr2.p.x[0] + (max_nzeros - limit);
		}
	}
	
	return;
}

// b - Ax
template<typename T>
void subtract(const Array<T> &b, const Array<T> &A_x, Array<T> &r, T coef, Context ctx, HighLevelRuntime *runtime){

        ArgumentMap arg_map;

	TaskArgs2<T> subtract_args(b, A_x, r, coef);
	
        IndexLauncher subtract_launcher(SUBTRACT_TASK_ID, b.color_domain,
                                    TaskArgument(&subtract_args, sizeof(subtract_args)), arg_map);

        subtract_launcher.add_region_requirement(
                        RegionRequirement(b.lp, 0, READ_ONLY, EXCLUSIVE, b.lr));
        subtract_launcher.region_requirements[0].add_field(b.fid);

        subtract_launcher.add_region_requirement(
                        RegionRequirement(A_x.lp, 0, READ_ONLY, EXCLUSIVE, A_x.lr));
        subtract_launcher.region_requirements[1].add_field(A_x.fid);

        subtract_launcher.add_region_requirement(
                        RegionRequirement(r.lp, 0, WRITE_DISCARD, EXCLUSIVE, r.lr));
        subtract_launcher.region_requirements[2].add_field(r.fid);

        runtime->execute_index_space(ctx, subtract_launcher);

	return;
}

template<typename T>
void subtract_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime){

        assert(regions.size() == 3);
        assert(task->regions.size() == 3);

        const TaskArgs2<T> task_args = *((const TaskArgs2<T>*)task->args);

	T alpha = task_args.scalar;

        RegionAccessor<AccessorType::Generic, T> acc_x =
        regions[0].get_field_accessor(task_args.x_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_y =
        regions[1].get_field_accessor(task_args.y_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_z =
        regions[2].get_field_accessor(task_args.z_fid).template typeify<T>();

        Domain dom = runtime->get_index_space_domain(ctx,
                         task->regions[0].region.get_index_space());
        Rect<1> rect = dom.get_rect<1>();

        // apply vector subtraction
        {
                GenericPointInRectIterator<1> itr1(rect);

                for(int i=0; i< rect.volume(); i++){
			T result = acc_x.read(DomainPoint::from_point<1>(itr1.p)) - 
					alpha * acc_y.read(DomainPoint::from_point<1>(itr1.p));
			
			 acc_z.write(DomainPoint::from_point<1>(itr1.p), result);
			 itr1++;
                 }
        }

        return;
}

// p = r_old
template<typename T>
void copy(const Array<T> &r, Array<T> &p, Context ctx, HighLevelRuntime *runtime){

        ArgumentMap arg_map;
	
	TaskArgs2<T> equal_args(r, p);

        IndexLauncher equal_launcher(COPY_TASK_ID, r.color_domain,
                                    TaskArgument(&equal_args, sizeof(equal_args)), arg_map);

        equal_launcher.add_region_requirement(
                        RegionRequirement(r.lp, 0, READ_ONLY, EXCLUSIVE, r.lr));
        equal_launcher.region_requirements[0].add_field(r.fid);

        equal_launcher.add_region_requirement(
                        RegionRequirement(p.lp, 0, WRITE_DISCARD, EXCLUSIVE, p.lr));
        equal_launcher.region_requirements[1].add_field(p.fid);

        runtime->execute_index_space(ctx, equal_launcher);
	return;
}

template<typename T>
void copy_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime){ 

        assert(regions.size() == 2);
        assert(task->regions.size() == 2);

        const TaskArgs2<T> task_args = *((const TaskArgs2<T>*)task->args);
                
        RegionAccessor<AccessorType::Generic, T> acc_x =
        regions[0].get_field_accessor(task_args.x_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_y =
        regions[1].get_field_accessor(task_args.y_fid).template typeify<T>();
                                    
        Domain dom = runtime->get_index_space_domain(ctx,
                         task->regions[0].region.get_index_space());
        Rect<1> rect = dom.get_rect<1>();

        // now y[i] = x[i]
        {
                GenericPointInRectIterator<1> itr1(rect);

                for(int i=0; i < rect.volume(); i++){

                        T result = acc_x.read(DomainPoint::from_point<1>(itr1.p));

                        acc_y.write(DomainPoint::from_point<1>(itr1.p), result);

                        itr1++;
                 }
        }

        return;
}

// x' * y
template<typename T>
T dot(const Array<T> &x, Array<T> &y, Context ctx, HighLevelRuntime *runtime){
	
        ArgumentMap arg_map;

	TaskArgs2<T> dot_args(x, y);

        IndexLauncher dot_launcher(DOT_TASK_ID, x.color_domain,
                                    TaskArgument(&dot_args, sizeof(dot_args)), arg_map);

        dot_launcher.add_region_requirement(
                        RegionRequirement(x.lp, 0, READ_ONLY, EXCLUSIVE, x.lr));
        dot_launcher.region_requirements[0].add_field(x.fid);

        dot_launcher.add_region_requirement(
                        RegionRequirement(y.lp, 0, READ_ONLY, EXCLUSIVE, y.lr));
        dot_launcher.region_requirements[1].add_field(y.fid);

        Future result = runtime->execute_index_space(ctx, dot_launcher, REDUCE_ID);
	
	return(result.get_result<T>());
}

template<typename T>
T  dot_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime){  

        assert(regions.size() == 2);
        assert(task->regions.size() == 2);

        const TaskArgs2<T> task_args = *((const TaskArgs2<T>*)task->args);
                
        RegionAccessor<AccessorType::Generic, T> acc_x =
        regions[0].get_field_accessor(task_args.x_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_y =
        regions[1].get_field_accessor(task_args.y_fid).template typeify<T>();

        Domain dom = runtime->get_index_space_domain(ctx,
                         task->regions[0].region.get_index_space());
        Rect<1> rect = dom.get_rect<1>();
	
	T sum = 0.0;
        // now apply the dot product using reduction operator!!!
        {
		GenericPointInRectIterator<1> itr1(rect);

		for(int i=0; i < rect.volume(); i++){
			sum += acc_x.read(DomainPoint::from_point<1>(itr1.p)) * 
				acc_y.read(DomainPoint::from_point<1>(itr1.p));

			itr1++;				
		}
        }
        return(sum);
}

// z = x + y
template<typename T>
void add(const Array<T> &x, const Array<T> &y, Array<T> &z, T coef, Context ctx, HighLevelRuntime *runtime){

        ArgumentMap arg_map;

	TaskArgs2<T> add_args(x, y, z, coef);

        IndexLauncher add_launcher(ADD_TASK_ID, x.color_domain,
                                    TaskArgument(&add_args, sizeof(add_args)), arg_map);

        add_launcher.add_region_requirement(
                        RegionRequirement(x.lp, 0, READ_ONLY, EXCLUSIVE, x.lr));
        add_launcher.region_requirements[0].add_field(x.fid);

        add_launcher.add_region_requirement(
                        RegionRequirement(y.lp, 0, READ_ONLY, EXCLUSIVE, y.lr));
        add_launcher.region_requirements[1].add_field(y.fid);

        add_launcher.add_region_requirement(
                        RegionRequirement(z.lp, 0, WRITE_DISCARD, EXCLUSIVE, z.lr));
        add_launcher.region_requirements[2].add_field(z.fid);

        runtime->execute_index_space(ctx, add_launcher);

	return;
}

template<typename T>
void add_task(const Task *task,
               const std::vector<PhysicalRegion> &regions,
               Context ctx, HighLevelRuntime *runtime){

        assert(regions.size() == 3);
        assert(task->regions.size() == 3);

        const TaskArgs2<T> task_args = *((const TaskArgs2<T>*)task->args);

	T alpha = task_args.scalar;

        RegionAccessor<AccessorType::Generic, T> acc_x =
        regions[0].get_field_accessor(task_args.x_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_y =
        regions[1].get_field_accessor(task_args.y_fid).template typeify<T>();

        RegionAccessor<AccessorType::Generic, T> acc_z =
        regions[2].get_field_accessor(task_args.z_fid).template typeify<T>();

        Domain dom = runtime->get_index_space_domain(ctx,
                         task->regions[0].region.get_index_space());
        Rect<1> rect = dom.get_rect<1>();

        // apply vector addition
        {
                GenericPointInRectIterator<1> itr1(rect);

                for(int i=0; i< rect.volume(); i++){
                        T result = acc_x.read(DomainPoint::from_point<1>(itr1.p)) +
                                        alpha * acc_y.read(DomainPoint::from_point<1>(itr1.p));

                         acc_z.write(DomainPoint::from_point<1>(itr1.p), result);
                         itr1++;
                 }
        }

        return;
}

template<typename T>
T L2norm(Array<T> &x, Context ctx, HighLevelRuntime *runtime){

	T norm = dot(x, x, ctx, runtime);
	return(sqrt(norm));
}

template<typename T>
static void RegisterOperatorTasks(void) {

	HighLevelRuntime::register_legion_task<spmv_task<T> >(SPMV_TASK_ID, 
	Processor::LOC_PROC, true/*single*/, true/*index*/);//,
	//AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "spmv");

	HighLevelRuntime::register_legion_task<subtract_task<T> >(SUBTRACT_TASK_ID,
        Processor::LOC_PROC, true/*single*/, true/*index*/);//,
        //AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "subtract");

	HighLevelRuntime::register_legion_task<copy_task<T> >(COPY_TASK_ID,
        Processor::LOC_PROC, true/*single*/, true/*index*/);//,
        //AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "equal");

	HighLevelRuntime::register_legion_task<T, dot_task<T> >(DOT_TASK_ID,
        Processor::LOC_PROC, true/*single*/, true/*index*/);//,
        //AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "dotproduct");

	HighLevelRuntime::register_reduction_op<FutureSum>(REDUCE_ID);

	HighLevelRuntime::register_legion_task<add_task<T> >(ADD_TASK_ID,
        Processor::LOC_PROC, true/*single*/, true/*index*/);//,
        //AUTO_GENERATE_ID, TaskConfigOptions(true/*leaf*/), "add");

	return;
}
#endif
